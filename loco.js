function tttCheckWin()
{
    td1 = document.getElementById("b1");
    td2 = document.getElementById("b2");
    td3 = document.getElementById("b3");
    td4 = document.getElementById("b4");
    td5 = document.getElementById("b5");
    td6 = document.getElementById("b6");
    td7 = document.getElementById("b7");
    td8 = document.getElementById("b8");
    td9 = document.getElementById("b9");
    board = [[td1.innerHTML, td2.innerHTML, td3.innerHTML],
             [td4.innerHTML, td5.innerHTML, td6.innerHTML],
             [td7.innerHTML, td8.innerHTML, td9.innerHTML]];
    for (i = 0; i < 3; i++){ // horizontales y verticales
        if (board[i][0] == board[i][1] && board[i][1] == board[i][2]){
            if (board[i][0] == 'x'){
                alert("Gana el jugador 1, refrescando en 2 segundos.");
                setTimeout("location.href = location.href", 2000);
            } else if (board[i][0] == 'o'){
                alert("Gana el jugador 2, refrescando en 2 segundos.");
                setTimeout("location.href = location.href", 2000);
            }
        } else if (board[0][i] == board[1][i] && board[1][i] == board[2][i]){
            if (board[0][i] == 'x'){
                alert("Gana el jugador 1, refrescando en en 2 segundos.");
                setTimeout("location.href = location.href", 2000);
            } else if (board[0][i] == 'o'){
                alert("Gana el jugador 2, refrescando en en 2 segundos.");
                setTimeout("location.href = location.href", 2000);
            }
        }
    } // diagonales
    if (board[0][0] == board[1][1] && board[1][1] == board[2][2]){
        if (board[0][0] == 'x'){
            alert("Gana el jugador 1, refrescando en en 2 segundos.");
            setTimeout("location.href = location.href", 2000);
        } else if (board[0][0] == 'o'){
            alert("Gana el jugador 2, refrescando en en 2 segundos.");
            setTimeout("location.href = location.href", 2000);
        }
    } else if (board[0][2] == board[1][1] && board[1][1] == board[2][0]){
        if (board[0][2] == 'x'){
            alert("Gana el jugador 1, refrescando en en 2 segundos.");
            setTimeout("location.href = location.href", 2000);
        } else if (board[0][2] == 'o'){
            alert("Gana el jugador 2, refrescando en en 2 segundos.");
            setTimeout("location.href = location.href", 2000);
        }
    }
    for (i = 0; i < 3; i++){
        for (i2 = 0; i2 < 3; i2++){
            if (board[i][i2].length == 0){
                return 0;
            }
        }
    }
    alert("Empate, refrescando en en 2 segundos.");
    setTimeout("location.href = location.href", 2000);
}
 
function tttOnClick(hidden, celda)
{
    cobj = document.getElementById(celda);
    if (cobj.innerHTML.length > 0){
        alert("No puedes poner la ficha en ese lugar.");
        return 0;
    }
    hobj = document.getElementById(hidden);
    player = 0;
    if (hobj.value == "p1"){
        player = 1;
        hobj.value = "p2";
        document.getElementById("turninfo").innerHTML = "Turno del jugador 2";
    } else if (hobj.value == "p2"){
        player = 2;
        hobj.value = "p1";
        document.getElementById("turninfo").innerHTML = "Turno del jugador 1";
    }
    if (player == 0){
        alert("No se ha encontrado el turno del jugador activo.");
        return 0;
    }
    switch (player){
    case 1:
        cobj.innerHTML = 'x';
        break;
    case 2:
        cobj.innerHTML = 'o';
        break;
    }
    cobj.className = "ttt_sel";            
    tttCheckWin();
}